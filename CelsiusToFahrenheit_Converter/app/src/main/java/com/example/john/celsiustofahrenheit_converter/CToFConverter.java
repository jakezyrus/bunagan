//07/15/17, Mobile Computing assignment

package com.example.john.celsiustofahrenheit_converter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;


public class CToFConverter extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cto_fconverter);
    }

    public void onButtonPress(View v)
    {
        //Bunagan, John Kenneth V.
        // Objects that seemed unaligned with other objects are intentional so that it will be aligned when i run it through my phone
        TextView txt_Fahrenheit = (TextView) findViewById(R.id.txt_Fahrenheit);
        EditText inp_Celsius = (EditText) findViewById(R.id.txt_Celsius);
        double Celsius = Double.parseDouble(inp_Celsius.getText().toString());
        double Fahrenheit = (Celsius * 9/5) + 32;
        txt_Fahrenheit.setText(Double.toString(Fahrenheit));
    }

}
